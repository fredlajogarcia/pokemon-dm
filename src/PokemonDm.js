import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './PokemonDm-styles.js';
import '@bbva-web-components/bbva-core-generic-dp/bbva-core-generic-dp.js';
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<pokemon-dm></pokemon-dm>
```

##styling-doc

@customElement pokemon-dm
*/
export class PokemonDm extends LitElement {
  static get is() {
    return 'pokemon-dm';
  }

  // Declare properties
  static get properties() {
    return {
      patho: { type: String, },
      pokemonID:{ type: String, },
      lista:{ type: Array, },
      pokemon:{ type: Object, },
      swlista:{ type: Boolean, },
      limite:{ type: String, },
      offset:{ type: String, },
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.patho = 'api/v2/pokemon/';
    this.pokemonID='1';
    this.lista={};
    this.pokemon=null;
    this.swlista=true;
    this.limite='151';
    this.offset="0";
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('pokemon-dm-shared-styles')
    ];
  }

  // Define a template
  render() {
    return html`
      <bbva-core-generic-dp
        id="pokemonDP"
        host="https://pokeapi.co"
        method="GET"
        @request-success="${this.requestSuccess}" 
        @request-error="${this.requestError}"
        >
        
      </bbva-core-generic-dp>

    `;

    
  }

  getListPokemons() {
    let dp = this.shadowRoot.getElementById('pokemonDP');
    dp.path = this.patho+"?limit="+this.limite+"&offset="+this.offset;
    this.swlista=true;
    dp.generateRequest();
  }

  requestSuccess(evt){
    if(this.swlista){
      this.lista = evt.detail.results;
      this.dispatchEvent(new CustomEvent('getPokemons-success', {
        detail: true,
        bubbles: true,
        composed: true,
      }));
    }else{
      this.pokemon = evt.detail;
      this.dispatchEvent(new CustomEvent('getPokemon-success', {
        detail: true,
        bubbles: true,
        composed: true,
      }));
    }
    

  }

  requestError(evt){
    this.dispatchEvent(new CustomEvent('getPokemons-error', {
      detail: true,
      bubbles: true,
      composed: true,
    }));
  }

  getPokemonDetail(){
    let dp = this.shadowRoot.getElementById('pokemonDP');
    dp.path = this.patho+this.pokemonID+"/";
    this.swlista=false;
    dp.generateRequest();
  }



}
